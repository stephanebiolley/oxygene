import { createRouter, createWebHistory } from 'vue-router'
import Home from '/src/components/Home.vue'
import MaterialList from '/src/components/MaterialList.vue'
const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
    },
    {
        path: '/material',
        name: 'Material',
        component: MaterialList,
    },
]
const router = createRouter({
    history: createWebHistory(),
    routes,
})
export default router
