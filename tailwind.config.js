module.exports = {
    purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            colors: {
                'oxygene-pink': '#E41F67',
            },
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
}
