/// <reference types="cypress" />

// Welcome to Cypress!
//
// This spec file contains a variety of sample tests
// for a todo list app that are designed to demonstrate
// the power of writing tests in Cypress.
//
// To learn more about how Cypress works and
// what makes it such an awesome testing tool,
// please read our getting started guide:
// https://on.cypress.io/introduction-to-cypress

describe('A member can use the app', () => {
    beforeEach(() => {
        cy.visit('/')
    })

    it('displays some categories', () => {
        cy.get('.category').should('have.length', 4)
        cy.get('.category').first().should('have.text', 'Stand Up Paddle')
        cy.get('.category').eq(1).should('have.text', 'Dériveur')
        cy.get('.category').eq(2).should('have.text', 'Planche à voile')
        cy.get('.category').last().should('have.text', 'Kayak')
    })
})
